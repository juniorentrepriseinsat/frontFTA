import {Component, OnDestroy, OnInit} from '@angular/core';
import {Centre} from '../shared/centre.model';
import {CentreDeFormationService} from '../_service/centre-de-formation.service';
import {Subscription} from 'rxjs';
import {HttpClient} from '@angular/common/http';


@Component({
    selector: 'app-centre-de-formation',
    templateUrl: './centre-de-formation.component.html',
    styleUrls: ['./centre-de-formation.component.scss'],
})

export class CentreDeFormationComponent implements OnInit, OnDestroy {

    centres: Centre[] = [];
    private postsSub: Subscription;


    constructor(private centreService: CentreDeFormationService, private http: HttpClient) {
    }

    ngOnInit() {
        /*this.centreService.getCentre();
        this.postsSub = this.centreService.getPostUpdateListener()
            .subscribe((centres: Centre[]) => {
                this.centres = centres;
            });
        this.centres = this.centreService.centres;*/

        this.centres.push(new Centre("1","Le Kef","Lotfi Hammami","97943225"));
        this.centres.push(new Centre("2","Gabes","Houcine Gouider","75398123"));
        this.centres.push(new Centre("3","Sidi Bouzid","Bechir Nciri","29933717"));
        this.centres.push(new Centre("4","Gafsa","Moncef Messaoued","29933715"));
        this.centres.push(new Centre("5","Rades","Abed Jelil Lahuiji","71468101 / 54859786"));
        this.centres.push(new Centre("6","Kebili","Lotfi Daroul","75494124"));
        this.centres.push(new Centre("7","Kairouan","Kamel Haouas","20557826 / 29933707"));



    }

    ngOnDestroy(): void {
        this.postsSub.unsubscribe();
    }


}

