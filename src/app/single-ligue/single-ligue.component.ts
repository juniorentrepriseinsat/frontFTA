import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Ligue} from '../_service/shared_files/ligue.model';
import {Club} from "../_service/shared_files/club.model";
import {LigueService} from "../_service/ligue.service";

import {Worker} from '../_service/shared_files/worker.model';
import {BureauService} from '../_service/bureau.service';


@Component({
  selector: 'app-single-ligue',
  templateUrl: './single-ligue.component.html',
  styleUrls: ['./single-ligue.component.scss']
})
export class SingleLigueComponent implements OnInit {
  ligueID;
  ligue: Ligue;

  workers = [];

  constructor(private route: ActivatedRoute, public ligueService: LigueService, public bureauService: BureauService) {
  }

  ngOnInit() {
    this.route.params.subscribe(
      data => {
        this.ligueID = data.id;
        this.ligueService.getLigue(this.ligueID).subscribe(data => {
          // @ts-ignore
          data = {id: data._id, ...data};
          this.ligue = new Ligue(data.id, data.name, data.president,
            data.email, data.mobile, data.fax, data.image);
        });
      }
    );
    window.scroll(0, 0);

    this.bureauService.getBureau().subscribe(dataa => {
      if (dataa != null) {
        dataa = dataa.map(x => {
          return {
            id: x._id,
            ...x
          };
        });
        for (const data of dataa) {
          this.workers.push(new Worker(data.id, data.name, data.dateNaissance, data.cin, data.tache,
            data.mobile, data.mobile2, data.email, data.image));
        }
      }
    });


  }
}
