import {Component, OnDestroy, OnInit} from '@angular/core';
import {GalerieList} from '../shared/galerieList.model';
import {GalerieService} from '../_service/galerie.service';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-gallerie',
    templateUrl: './gallerie.component.html',
    styleUrls: ['./gallerie.component.scss']
})
export class GallerieComponent implements OnInit, OnDestroy {

    galerie: GalerieList[] = [];
    private postsSub: Subscription;


    constructor(private galerieService: GalerieService) {
    }


    ngOnInit() {
        this.galerieService.getGalerieList().subscribe(dataa => {
      dataa = dataa.map(x => {
        return {
          id: x._id,
          ...x
        };
      });
      for (const data of dataa) {
        this.galerie.push(new GalerieList(data.id, data.galeriePath, data.galerieTitle,
          data.galerieImgPath));
      }
      window.scroll(0, 0);
    });
    }

    ngOnDestroy(): void {
        this.postsSub.unsubscribe();
    }

}
